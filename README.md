#OrangePI System#

* To create SDCARD
Using linux
```
sudo dd=if=./debian_desktop_For_OrangePizero_v0_8_2.img of=/dev/sdX
``` 

######Onde X é a letra do dispositivo detectado, no meu computador foi sdc######
######Verifique também se o SDcard não está montado no ato da gravação e lembre-se de não usar números correspondente às partições, ex: sdc1, sdc2######


* To enable VNC

```
vim /etc/lightdm/lightdm.conf
```

go to [VNCServer]
and change 

```
[VNCServer]
enabled=true
command=Xvnc
port=5900
width=1024
height=768
depth=24
```